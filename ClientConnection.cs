﻿/*
Savagedlight.Network.Server
Copyright (c) 2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using Savagedlight.Network;
using Savagedlight.Network.Exceptions;
using Savagedlight.Network.Packets;
using Savagedlight.Network.Server.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Savagedlight.Network.Server
{
    internal class ClientConnection
    {
        private NetworkServer Server { get; set; }
        internal BlockingCollection<NetworkSendQueueEntry> ToWrite { get; private set; }
        private Task WriterTask { get; set; }

        private TcpClient Client { get; set; }
        internal event ServerEventHandler<ClientDataEventArgs> DataReceivedEvent;
        internal event ServerEventHandler<ClientConnectionStateEventArgs> ConnectionStateEvent;
        public Guid Id { get; private set; }
        public ConnectionState State { get; private set; }
        private Stopwatch LastActivity { get; set; }
        public int PingTime { get; set; }

        public ClientConnection(NetworkServer server, TcpClient client)
        {
            this.PingTime = 5000;
            this.Server = server;
            this.ToWrite = new BlockingCollection<NetworkSendQueueEntry>();
            this.Id = Guid.NewGuid();
            this.Client = client;
        }

        public void Start()
        {
            this.State = ConnectionState.Connected;
            this.LastActivity = Stopwatch.StartNew();
            // Start writer
            this.WriterTask = Task.Factory.StartNew(this.Writer, TaskCreationOptions.LongRunning);
            // Start async reader            
            this.Read();            
        }

        private bool waitingForPong = false;
        
        private async void Read()
        {
            byte[] buffer;
            int pos;
            try
            {
                using (NetworkStream stream = this.Client.GetStream())
                {
                    while (this.State == ConnectionState.Connected)
                    {
                        // Get a short describing packet length
                        buffer = new byte[4];
                        pos = 0;
                        do
                        {
                            int read = await stream.ReadAsync(buffer, pos, buffer.Length - pos);
                            pos += read;
                            if (read == 0)
                            {
                                throw new IOException();
                            }
                        } while (pos < buffer.Length);

                        // Get the length.
                        ushort length = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(buffer, 0));
                        ushort packetId = (ushort)IPAddress.NetworkToHostOrder(BitConverter.ToInt16(buffer, 2));


                        // Check if the packet is valid
                        if (!this.Server.PacketHandler.IsValidPacketId(packetId))
                        {
                            throw new InvalidPacketIdException(packetId);
                        }

                        if (!this.Server.PacketHandler.IsValidPacketDirection(packetId, PacketDirection.ClientServer))
                        {
                            throw new InvalidDirectionPacketIdException(packetId, PacketDirection.ClientServer);
                        }

                        buffer = new byte[length];
                        pos = 0;
                        if (length > 0)
                        {
                            // Retrieve the payload
                            do
                            {
                                int read = await stream.ReadAsync(buffer, pos, buffer.Length - pos);
                                pos += read;
                                if (read == 0)
                                {
                                    throw new IOException();
                                }
                            } while (pos < buffer.Length);
                        }

                        this.LastActivity.Restart();
                        // Create packet
                        Packet packet = this.Server.PacketHandler.DeserializePacket(packetId, buffer);
                        if (packet.Id <= Packet.HighestReservedPacketId)
                        {
                            this.ProcessReservedPacket(packet);
                            continue;
                        }
                        else
                        {
                            // Send data through event handler
                            var evnt = this.DataReceivedEvent;
                            if (evnt != null)
                            {
                                evnt(null, new ClientDataEventArgs(this.Id, packet));
                            }
                        }
                    }
                }
            }
            catch (InvalidPacketIdException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (InvalidDirectionPacketIdException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (ObjectDisposedException) { }
            catch (IOException ex) { }
            finally
            {
                if (this.Client.Connected)
                {
                    this.Client.Close();
                }
                this.ForceDisconnect();
            }
        }

        private void Writer()
        {
            Thread.CurrentThread.Name = "[" + this.Id.ToString() + "] ClientConnection->Writer()";
            Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
            try
            {
                using (NetworkStream stream = this.Client.GetStream())
                {
                    while (!this.ToWrite.IsCompleted)
                    {
                        NetworkSendQueueEntry nsqe;
                        if (!this.ToWrite.TryTake(out nsqe, this.PingTime))
                        {
                            CheckPing();
                            continue;
                        }

                        if (this.State == ConnectionState.Disconnected)
                        {
                            return;
                        }
                        CheckPing();

                        nsqe.Write(stream);

                        if (nsqe.PacketId == Packet.DisconnectPacket)
                        {
                            return;
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (ObjectDisposedException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (this.Client.Connected)
                {
                    this.Client.Close();
                }
                this.ForceDisconnect();
            }
        }

        private void CheckPing()
        {
            if (this.LastActivity.ElapsedMilliseconds < this.PingTime)
            {
                return;
            }
            else if (this.LastActivity.ElapsedMilliseconds >= this.PingTime)
            {
                if (this.waitingForPong)
                {
                    this.Server.Write(this.Id, new DisconnectPacket("Ping Timeout"));
                    return;
                }
                else
                {
                    this.Server.Write(this.Id, new PingPacket());
                    this.waitingForPong = true;
                    this.LastActivity.Restart();
                    return;
                }
            }
            return;
        }

        internal void ForceDisconnect()
        {
            switch (this.State)
            {
                case ConnectionState.Disconnected:
                    return;
                case ConnectionState.Disconnecting:
                case ConnectionState.Connected:
                    if (this.Client.Connected)
                    {
                        this.Client.Close();
                    }
                    this.ToWrite.CompleteAdding();
                    this.State = ConnectionState.Disconnected;                    

                    var evnt = this.ConnectionStateEvent;
                    if (evnt != null)
                    {
                        lock (evnt)
                        {
                            evnt(null, new ClientConnectionStateEventArgs(this.Id, ConnectionState.Disconnected));
                        }
                    }
                    break;
            }
        }

        internal static async Task<bool> GetHandshake(TcpClient client, byte[] handshake)
        {            
            int org = client.ReceiveTimeout;
            client.ReceiveTimeout = 100;

            NetworkStream stream = client.GetStream();
            byte[] buffer = new byte[handshake.Length];
            int pos = 0;
            Stopwatch sw = Stopwatch.StartNew();
            while (pos < buffer.Length)
            {
                if (sw.ElapsedMilliseconds >= 100) { return false; }
                pos += await stream.ReadAsync(buffer, pos, buffer.Length - pos);
            }
            client.ReceiveTimeout = org;
            return handshake.SequenceEqual(buffer);
        }

        #region Handle incoming default packets
        private void ProcessReservedPacket(Packet packet)
        {
            switch (packet.Id)
            {
                case Packet.DisconnectPacket:
                    this.ProcessDisconnectPacket((DisconnectPacket)packet);
                    break;
                case Packet.PingPacket:
                    Console.WriteLine("[{0}]: Ping?", this.Id);
                    Console.WriteLine("To [{0}]: Pong!", this.Id);
                    this.Server.Write(this.Id, new PongPacket());
                    break;
                case Packet.PongPacket:
                    Console.WriteLine("[{0}]: Pong!", this.Id);
                    this.waitingForPong = false;
                    break;
            }
        }
        private void ProcessDisconnectPacket(DisconnectPacket packet)
        {

        }
        #endregion
    }
}

