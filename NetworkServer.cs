﻿/*
Savagedlight.Network.Server
Copyright (c) 2014 Savagedlight <marieheleneka@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using Savagedlight;
using Savagedlight.Serialization;
using Savagedlight.Network;
using Savagedlight.Network.Packets;
using Savagedlight.Network.Server.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Savagedlight.Network.Server
{
    public class NetworkServer
    {
        private TcpListener serverSocket;
        private ConcurrentDictionary<Guid, ClientConnection> clients = new ConcurrentDictionary<Guid, ClientConnection>();

        public event ServerEventHandler<ClientConnectionStateEventArgs> ClientConnectionState;
        public event ServerEventHandler<ClientDataEventArgs> ClientDataReceived;
        private BlockingCollection<ServerEventArgs> Events { get; set; }
        private Task EventDispatcherTask { get; set; }
        internal PacketHandler PacketHandler { get; private set; }

        public byte[] Handshake { set; private get; }

        public IPEndPoint Listen { get; set; }

        private NetworkServer()
        {
            this.Handshake = new byte[] { 1, 3, 5, 7 };
            IPAddress ipAddress = IPAddress.Loopback;
            this.Listen = new IPEndPoint(ipAddress, 11000);
        }

        public NetworkServer(PacketHandler packetHandler)
            : this()
        {
            this.PacketHandler = packetHandler;            
        }

        public async void Start()
        {
            this.Events = new BlockingCollection<ServerEventArgs>();
            this.EventDispatcherTask = Task.Factory.StartNew(this.EventDispatcher, TaskCreationOptions.LongRunning);
            // Create a TCP/IP socket.
            serverSocket = new TcpListener(this.Listen);
            // Bind the socket to the local endpoint and listen for incoming connections.
            serverSocket.Start(100);
            Console.WriteLine("Listening to {0}", Listen.ToString());

            while (true)
            {
                try
                {
                    var tcpClient = await this.serverSocket.AcceptTcpClientAsync();
                    await ProcessIncomingConnection(tcpClient);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: {0}", ex.ToString());
                }
            }
        }

        private async Task ProcessIncomingConnection(TcpClient tcpClient)
        {
            bool accept = await ClientConnection.GetHandshake(tcpClient, this.Handshake);

            if (!accept)
            {
                Console.WriteLine("Client attempted to connect, but handshake was not correct.");
                tcpClient.Close();
                return;
            }

            ClientConnection client = new ClientConnection(this, tcpClient);
            if (!this.clients.TryAdd(client.Id, client))
            {
                tcpClient.Close();
                Console.WriteLine("Failed to register client: Duplicate ID");
                return;
            }

            client.DataReceivedEvent += ProcessClientEvent;
            client.ConnectionStateEvent += ProcessClientEvent;
            Console.WriteLine("[{0}]: Connected from {1}", client.Id, tcpClient.Client.RemoteEndPoint.ToString());
            client.Start();
            this.EnqueueServerEvent(new ClientConnectionStateEventArgs(client.Id, ConnectionState.Connected));
        }

        private void EnqueueServerEvent(ServerEventArgs e)
        {
            this.Events.Add(e);
        }
        #region Workers
        private void EventDispatcher()
        {
            foreach (ServerEventArgs e in this.Events.GetConsumingEnumerable())
            {
                switch (e.Type)
                {
                    case ServerEventType.ClientConnectionState:                        
                        SendEvent(this.ClientConnectionState, (ClientConnectionStateEventArgs)e);
                        break;
                    case ServerEventType.ClientData:
                        SendEvent(this.ClientDataReceived, (ClientDataEventArgs)e);
                        break;
                }
            }
        }        

        private void SendEvent<T>(ServerEventHandler<T> handler, T e)
            where T : ServerEventArgs
        {
            if (handler == null) { return; }
            lock (handler)
            {
                handler(this, e);
            }
        }
        #endregion

        #region API
        /// <summary>
        /// Write <paramref name="data"/> to all clients specified in <paramref name="clients"/>
        /// Supports byte array, and conversion using StreamData.
        /// </summary>
        /// <param name="clients"></param>
        /// <param name="data"></param>
        public void Write(Guid[] clients, Packet packet)
        {
            byte[] toWrite = this.PacketHandler.SerializePacket(packet);
            var q = new NetworkSendQueueEntry(packet.Id, toWrite);
            foreach (Guid client in clients)
            {
                ClientConnection cc;
                if (!this.clients.TryGetValue(client, out cc)) { continue; }
                cc.ToWrite.Add(q);
            }
        }

        /// <summary>
        /// Write <paramref name="data"/> to <paramref name="client"/>. <br />
        /// Supports byte array, and conversion using StreamData.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="data"></param>
        public void Write(Guid client, Packet packet)
        {
            ClientConnection cc;
            if (!this.clients.TryGetValue(client, out cc)) 
            { 
                throw new Exception("Invalid client"); 
            }

            if (cc.State != ConnectionState.Connected)
            {
                throw new Exception("Client isn't connected. Cannot queue new packets.");
            }

            byte[] toWrite = this.PacketHandler.SerializePacket(packet);
            var q = new NetworkSendQueueEntry(packet.Id, toWrite);
            cc.ToWrite.Add(q);
        }


        /// <summary>
        /// Disconnect immediately.
        /// </summary>
        /// <param name="client"></param>
        public void Disconnect(Guid client)
        {
            ClientConnection cc;
            if (!this.clients.TryGetValue(client, out cc))
            {
                throw new Exception("Invalid client");
            }
            cc.ForceDisconnect();
        }

        /// <summary>
        /// Gracefully disconnect
        /// </summary>
        /// <param name="client"></param>
        /// <param name="message"></param>
        public void Disconnect(Guid client, string message)
        {
            this.Write(client, new DisconnectPacket(message));
        }
        #endregion

        #region Client event processing

        private void ProcessClientEvent(NetworkServer server, ServerEventArgs e)
        {
            switch (e.Type)
            {
                case ServerEventType.ClientData:
                    this.HandleClientData((ClientDataEventArgs)e);
                    break;
                case ServerEventType.ClientConnectionState:
                    this.HandleClientConnectionState((ClientConnectionStateEventArgs)e);
                    break;
            }
        }

        private void HandleClientData(ClientDataEventArgs e)
        {
            switch (e.Packet.Id)
            {
                // Do special handling of packets here.
            }
            this.EnqueueServerEvent(e);
        }

        private void HandleClientConnectionState(ClientConnectionStateEventArgs e)
        {
            Console.WriteLine("[{0}]: {1}", e.Client.ToString(), e.State.ToString());
            switch (e.State)
            {
                case ConnectionState.Disconnected:
                    ClientConnection conn;
                    this.clients.TryRemove(e.Client, out conn);
                    break;
            }
        }
        #endregion
    }
}
